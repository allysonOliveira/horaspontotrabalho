package com.horarioPonto.Trabalho.model;

import lombok.*;
import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Builder
@Entity
@Table(name = "ENDERECO_USUARIO")
public class EnderecoUsuario {

    @Id
    private Long idEndereco;
    private String endereco;
    private String bairro;
    private String numeroCasa;
    private String cidade;
    private String estado;
}
